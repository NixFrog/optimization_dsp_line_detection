solution "projet_optimisation"
	configurations ""
	project "video_flux_optimisation"
	kind "ConsoleApp"
	language "C++"
	files({ "src/*.h", "src/*.cpp" })
	--includedirs({"/", "includes"})

	flags({"Symbols", "ExtraWarnings"})
	links({"gomp"})
	--libdirs({"Driver/"})

	buildoptions({"-Ofast", "-std=c++11", "`pkg-config --cflags opencv`", "-march=native", "-mtune=native", "-pg", "-fopenmp"})
	linkoptions({"-std=c++11", "-lpthread", "-lm", "`pkg-config --libs opencv`", "-pg", "-lm", "-fopenmp"})
