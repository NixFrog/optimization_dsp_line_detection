#include <iostream>
#include <chrono>

/* Libraries OPENCV */
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

/* Own headers */
#include "Sobel.hpp"
#include "Smooth.hpp"

Sobel sobel_;
Smooth smooth_(30, 640, 480, 15, 8);

/*--------------- MAIN FUNCTION ---------------*/
int main(int argc, char const *argv[]){
	if(argc !=2){
		std::cout<<"Wrong number of parameters: ./prog <r>"<<std::endl;
		exit(EXIT_FAILURE);
	}
	int r = atoi(argv[1]);

	cv::VideoCapture cap(0);

	if(!cap.isOpened()){
		std::cerr << "Error capturing video flux"; return -1;
		return EXIT_FAILURE;
	}

	cv::Mat3b frame;
	cv::Mat frame_gray;
	cv::Mat frame_deriche;
	cv::Mat frame_hough;

	char key = '0';

	cap.read(frame);
	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, frame_deriche, CV_BGR2GRAY);
	cvtColor(frame, frame_hough, CV_BGR2GRAY);

	for(int i=0; i<r && key!='q'; i++){
		cap.read(frame);

		cvtColor(frame, frame_gray, CV_BGR2GRAY);

		smooth_.dericheGL(frame_gray, frame_deriche);
		smooth_.hough(frame_deriche);

		// imshow("Input",frame);
		// imshow("Output Gray",frame_gray);
		imshow("Output Deriche",frame_deriche);
		// imshow("Output Hough",frame_hough);

		key=cv::waitKey(5);
	}

	cvDestroyWindow("Input");
	cvDestroyWindow("Output Gray");
	cvDestroyWindow("Output Deriche");
	cvDestroyWindow("Output Hough");

	return EXIT_SUCCESS;
}
