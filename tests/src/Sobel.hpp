#ifndef __SOBEL_HPP__
#define __SOBEL_HPP__

#include <iostream>
#include <cmath>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

// types definitions
typedef unsigned char uint8;

class Sobel {

public:
	Sobel();
	int sobelMatrixApplication_opti(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22);

	void sobelFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl);

};
#endif //__SOBEL_HPP__
