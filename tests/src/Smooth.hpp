#ifndef __SMOOTH_HPP__
#define __SMOOTH_HPP__

#include <iostream>
#include <cmath>
#include <algorithm>
#include <chrono>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAX_VALUE_BIN 0xFF
#define MIN_VALUE_BIN 0x00
#define LOOKUP_SIZE 361
#define VOTES_LINE 60
#define PI 3.14159

// types definitions
typedef unsigned char uint8;

class Smooth {

public:
	Smooth(uint8_t gamma, size_t width, size_t height, uint8_t threshold, uint8_t powerOfG);
	~Smooth();
	void dericheGL(const cv::Mat& in, cv::Mat& out);
	void hough(cv::Mat& out);

private:
	void smoothing(uint8_t* img, uint8_t* C, uint8_t* A);
	inline void smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2);
	int lutArctan(double angle);

	uint8_t* causal_;
	uint8_t* anticausal_;
	uint8_t* tmp_;
	uint8_t* acc_;
	int* lutCos_;
	int* lutSin_;
	int* lutArctan_;

	uint8_t gamma_;
	size_t 	width_, height_;
	uint8_t threshold_;
	uint8_t powerOfG_;

	template<typename T>
	inline
	T* getPos(T* img, const int y, const int x){
		return img + (x + y*width_);
	}
};
#endif // __SMOOTH_HPP__
