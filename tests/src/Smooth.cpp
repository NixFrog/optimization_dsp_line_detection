#include "Smooth.hpp"

Smooth::Smooth(uint8_t gamma, size_t width, size_t height, uint8_t threshhold, uint8_t powerOfG)
    : gamma_(gamma)
    , width_(width)
    , height_(height)
    , threshold_(threshhold)
    , powerOfG_(powerOfG)
{
    causal_     = (uint8_t*) malloc(width_*height_*sizeof(uint8_t));
    anticausal_ = (uint8_t*) malloc(width_*height_*sizeof(uint8_t));
    tmp_        = (uint8_t*) malloc(width_*height_*sizeof(uint8_t));

    size_t maxrho  = ceil(sqrt(width_*width_ + height_*height_));
    acc_           = (uint8_t*) malloc(maxrho*180*sizeof(uint8_t));

    lutCos_ = (int*) malloc(LOOKUP_SIZE*sizeof(int));
    lutSin_ = (int*) malloc(LOOKUP_SIZE*sizeof(int));
    lutArctan_ = (int*) malloc(231*sizeof(int));

    for(auto i=-180; i<180; i++){
        lutCos_[i] = std::cos(i*PI/180)*256;
        lutSin_[i] = std::sin(i*PI/180)*256;
    }

    for(auto i = -115; i< 116; i++){
        lutArctan_[i] = std::atan(i)*180/PI;
    }
}

int Smooth::lutArctan(double angle)
{
    if(angle < 0.009 && angle > -0.009){
        return 0;
    }
    else if(angle < 1 && angle > -1){
        return std::atan(angle)*180/PI;
    }
    else if(angle > 115){
        return 90;
    }
    else if(angle < -115){
        return -90;
    }
    else{
        return lutArctan_[int(angle)];
    }
}


Smooth::~Smooth()
{
    free(tmp_);
    free(anticausal_);
    free(causal_);
    free(lutArctan_);
}

void Smooth::hough(cv::Mat& out)
{
    auto begin = std::chrono::high_resolution_clock::now();

    uint8_t *im_out;
    size_t maxrho = ceil(sqrt(width_*width_ + height_*height_));

	im_out = (uint8_t*)(out.data);

    for(size_t i = 0; i < maxrho; i++){
        for(size_t j = 1; j<180; j++){
            if(acc_[i*180+j] > VOTES_LINE){
                int rho  = ((i<<1) - maxrho)<<8;
                int a = - (lutCos_[j] / lutSin_[j])<<8;
                int b = (rho / lutSin_[j])<<8;

                if(j >= 45 && j <= 135){
                    for(size_t x=0; x<width_; x++){
                        int y = (a*x+b)>>8;
                        if(y>0 && y<=height_){
                            im_out[y*width_+x] = 255;
                        }
                    }
                }
                else{
                    for(size_t y=0; y<height_; y++){
                        int x = 0;
                        if(a != 0){
                            x = ((y<<8)-b)/a;
                        }
                        if(x>=0 && x<width_){
                            im_out[y*width_+x] = 255;
                        }
                    }
                }
            }
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Draw " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns" << std::endl;
}

void Smooth::dericheGL(const cv::Mat& in, cv::Mat& out)
{
    auto begin = std::chrono::high_resolution_clock::now();

    uint8_t* im_in, * im_out, * ptrAn, * ptrTmp, * ptrCa, * ptrOut;
    size_t i, ilen;
    int dx, dy;
    uint8_t maxVal = 1;

    im_in  = (uint8_t*)(in.data);
	im_out = (uint8_t*)(out.data);

    smoothing(im_in, causal_, anticausal_);

    ptrAn  = &anticausal_[0];
    ptrTmp = &tmp_[0];
    ptrCa  = &causal_[0];

    for(i = 0, ilen = height_ * width_; i < ilen; ++i){
        if(*ptrAn > 255.0){
            *ptrTmp = 255.0;
        }
        else{
            *ptrTmp = *ptrAn;
        }
        *ptrCa = *ptrTmp;

        ptrAn++;
        ptrTmp++;
        ptrCa++;
    }

    smoothing(tmp_, causal_, anticausal_);


    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Smooth " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns" << std::endl;

    begin = std::chrono::high_resolution_clock::now();

    ptrAn  = &anticausal_[0];
    ptrOut = &im_out[0];

    size_t maxrho  = ceil(sqrt(width_*width_ + height_*height_));
    memset(acc_, 0, maxrho*180);

    for(i = 0, ilen = height_ * width_; i < ilen; ++i){
        unsigned int sum;
        int theta, rho, rhoIdx;

        dx = (
            - *(ptrAn)
            + *(ptrAn + 1)
            - *(ptrAn + width_)
            + *(ptrAn + width_ + 1)
        );

        dy =(
            - *(ptrAn)
            + *(ptrAn + width_)
            - *(ptrAn + 1)
            + *(ptrAn + width_ + 1)
        );

        sum = abs(dx)+abs(dy);
        *ptrOut = im_in[i];

        if(sum > threshold_ && dx != 0){
            size_t x = i%width_;
            size_t y = ((i-x)/width_);

            theta = lutArctan(double(dy)/double(dx));

            rho   = ((x<<8)*lutCos_[theta] + (y<<8)*lutSin_[theta])>>16;
            rhoIdx = ((rho>>1) + (maxrho>>1));

            if(rhoIdx > 0){
                uint8_t accVal = acc_[rhoIdx*180+theta]+1;
                acc_[rhoIdx*180+theta] = accVal;
                maxVal = accVal > maxVal ? accVal : maxVal;
            }
        }

        ptrAn++;
        ptrOut++;
    }

    end = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulator " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() << "ns" << std::endl;


    for(i = 0; i<180*maxrho; i++){
        acc_[i] = acc_[i]*255/maxVal;
    }
}

void Smooth::smoothing(uint8_t* img, uint8_t* C, uint8_t* A)
{
    int g  = gamma_;
    int g1 = ((1<<powerOfG_) - g)*((1<<powerOfG_) - g);
    int g2 = g<<1;
    int gg = g*g;
    int i;

    uint8_t powerOfG2 = powerOfG_<<1;

    for(i = 0; i < height_; i+=4){
        smoothingLoopContent(img, C, A, g1, g2, gg, i+0, (i+0)*width_, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+1, (i+1)*width_, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+2, (i+2)*width_, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+3, (i+3)*width_, powerOfG2);
    }
}

void Smooth::smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2)
{
    int j;

    uint8_t* ptrI = &img[iw];

    uint8_t* ptrC = &C[iw];
    uint8_t* ptrCm2, * ptrCm1, * ptrC0, * ptrC1, * ptrC2;

    uint8_t* ptrA;
    uint8_t* ptrA1, * ptrA2, * ptrA3, * ptrA4;

    *ptrC++ = (g1*(*ptrI++))>>powerOfG2;
    *ptrC   = (g1*(*ptrI++) + g2*(*(ptrC-1) ))>>powerOfG2;
    ptrC++;

    for(j = 2; j < width_; j+=4){
        ptrCm2 = ptrC-2;
        ptrCm1 = ptrC-1;
        ptrC0  = ptrC;
        ptrC1  = ptrC+1;
        ptrC2  = ptrC+2;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrCm1)
            + gg * (*ptrCm2)
        )>>powerOfG2;

        ptrI++;

        *ptrC1 = (
              g1 * (*ptrI)
            + g2 * (*ptrC0)
            + gg * (*ptrCm1)
        )>>powerOfG2;

        ptrI++;

        *ptrC2 = (
              g1 * (*ptrI)
            + g2 * (*ptrC1)
            + gg * (*ptrC0)
        )>>powerOfG2;

        ptrC+=3;
        ptrI++;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrC2)
            + gg * (*ptrC1)
        )>>powerOfG2;

        ptrC++;
        ptrI++;
    }

    ptrA = &A[iw + width_ - 1];
    ptrC = &C[iw - 1];

    *ptrA-- = (g1 * (*ptrC--))>>powerOfG2;
    *ptrA   = (g1 * (*ptrC--) + g2 * (*ptrA))>>powerOfG2;
    ptrA--;

    for(j = width_ - 3; j>=0; j-=4){
        ptrA1 = ptrA+1;
        ptrA2 = ptrA+2;
        ptrA3 = ptrA+3;
        ptrA4 = ptrA+4;

        *ptrA = (
              g1 * (*ptrC)
            + g2 * (*ptrA1)
            + gg * (*ptrA2)
        )>>powerOfG2;

        ptrC++;

        *ptrA1 = (
              g1 * (*ptrC)
            + g2 * (*ptrA2)
            + gg * (*ptrA3)
        )>>powerOfG2;

        ptrC++;

        *ptrA2 = (
              g1 * (*ptrC)
            + g2 * (*ptrA3)
            + gg * (*ptrA4)
        )>>powerOfG2;

        ptrC++;

        *ptrA3 = (
              g1 * (*ptrC)
            + g2 * (*ptrA4)
            + gg * (*(ptrA + 5))
        )>>powerOfG2;

        ptrC++;
        ptrA -= 4;
    }
}
