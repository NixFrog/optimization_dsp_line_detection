function Acc = hough_exh(Ib)

[m,n]=size (Ib);
cy = round(m/2); % y center
cx = round(n/2); % x center
maxrho = ceil ((sqrt(2.0)*max(m,n))/2);
% Accumulator initialization
Acc = zeros (maxrho*2,180);
% Compute Hough transform for every Ib(x,y)>0
for i=2:m-1,% i => y coordinates
    for j=2:n-1,% j => x coordinates
        if Ib(i,j) > 0,
            for theta = 0:179,
               rho = (j-cx)*cosd(theta) + (i-cy)*sind(theta);
               rho_idx = ceil(rho + maxrho);
               Acc(rho_idx,theta+1) = Acc(rho_idx,theta+1) + 1;
            end;
        end;
    end;
end;