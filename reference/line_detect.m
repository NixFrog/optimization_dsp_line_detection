close all;
clear all;
video_on = 1;
if video_on == 0,
% load image and convert it in gray levels
RGB = imread('test11.bmp');
I  = rgb2gray(RGB); % convert to intensity
else
    cam = webcam(1);
RGB = snapshot(cam);
I = rgb2gray(RGB);
end;
%---------
% show I
%---------
im_show(I,'Original image');

%-------------------------------
% define application constants
%-------------------------------
gamma = 0.2
th  = 90;
votes = 80;

%---------------------------------------------
% Deriche filter (Gracia Lorca Optimization)
%---------------------------------------------
% smoothing
Is = smoothing_GL(I,gamma);
% Roberts gradient
[Igx,Igy] = roberts(Is);
Ig = sqrt(Igx.^2 + Igy.^2);
%---------
% show Is & Is 
%---------
im_show(Is,'Smoothed image');
im_show(Ig,'Gradient');

%---------------------------------------------
% Contours detection by binarization
%---------------------------------------------
Ib = Ig > th;
%---------
% show Ib
%---------
im_show(Ib, 'Binary contours');

%----------------------------------------------
% Hough transform
%----------------------------------------------
Acc = hough_exh (Ib);

%---------
% show Acc
%---------
surfl(Acc);
shading interp;

%----------------------------------------------
% Hough peak detection and line drawing
%----------------------------------------------
% optional peak number reduction
Accmax = imregionalmax(Acc);
Accb = (Accmax.*Acc);
im_show(Accb,'Hough peaks');
hough_peaks_lines(Accb,RGB,votes);

if video_on == 0;
    clear cam;
end;