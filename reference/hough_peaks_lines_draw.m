function hough_peaks_lines (Acc, RGB,votes)

[m,n] = size(Acc);
[mi,ni,k] = size(RGB);
im_lines = zeros(mi,ni);
v = votes;
%---------
% draw Hough lines
%---------
im_show(RGB,'Hough lines');
hold on;
axis([0 ni 0 mi])
for r = 1 : m, % r => y coordinate
    for t = 1 : n, %t => x coordinate
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        if Acc(r,t)>=v,
            if (t >= 45) && (t<=135)
                %y = (r - x cos(t)) / sin(t)
                x1 = 0;
                y1 = ((r - (m/2)) - (x1 - (ni/2))*cosd(t))/sind(t) + (mi/2);
                x2 = ni;
                y2 = ((r - (m/2)) - (x2 - (ni/2))*cosd(t))/sind(t) + (mi/2);
                %line ([x1 x2],[y1 y2]);
                b = y1;
                a = (y2 - b)/x2;
                for x = 1 : ni,
                    y = round(a*x + b);
                    if (y>0)&&(y<=mi),
                        im_lines(y,x)=255;
                    end;
                end;
            else
                %x = (r - y sin(t)) / cos(t)
                y1 = 0;
                x1 = ((r - (m/2)) - (y1 - (mi/2))*sind(t))/cosd(t) + (ni/2);
                y2 = mi;
                x2 = ((r - (m/2)) - (y2 - (mi/2))*sind(t))/cosd(t) + (ni/2);
                %line ([x1 x2],[y1 y2]);
                a = (y2-y1)/(x2-x1);
                b = y2 - (a*x2);

                for y = 1 : mi,
                    x = round((y - b)/a);
                    if (x>0)&&(x<=ni),
                        im_lines(y,x)=255;
                    end;
                end;
            end;
        end;
    end;
end;
hold off;
im_show(im_lines,'Lines');
