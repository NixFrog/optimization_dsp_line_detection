/* T. Grandpierre :
Version initiale qui ne fait que acquisition YUV puis affichage
*/

#include <std.h>
#include <gio.h>
#include <log.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "psp_vpfe.h"
#include "psp_vpbe.h"
#include "fvid.h"
#include "psp_tvp5146_extVidDecoder.h"

#include <soc.h>
#include <cslr_ccdc.h>

extern LOG_Obj trace;  // BIOS LOG object

#define NO_OF_BUFFERS       (2u)

// Global Variable Defined
static PSP_VPSSSurfaceParams *ccdcAllocFB[NO_OF_BUFFERS]={NULL};
static PSP_VPSSSurfaceParams *vidAllocFB[NO_OF_BUFFERS] ={NULL};

static FVID_Handle   ccdcHandle;
static FVID_Handle   vid0Handle;
static FVID_Handle   vencHandle;

static PSP_VPFE_TVP5146_ConfigParams tvp5146Params = {
  TRUE, // enable656Sync
  PSP_VPFE_TVP5146_FORMAT_COMPOSITE, // format
  PSP_VPFE_TVP5146_MODE_AUTO         // mode
};

static PSP_VPFECcdcConfigParams ccdcParams = {
  PSP_VPFE_CCDC_YCBCR_8,  // dataFlow
  PSP_VPSS_FRAME_MODE,    // ffMode
  480,                    // height
  720,                    // width
  (720 *2),               // pitch
  0,                      // horzStartPix
  0,                      // vertStartPix
  NULL,                   // appCallback
  {
    PSP_VPFE_TVP5146_Open, // extVD Fxn
    PSP_VPFE_TVP5146_Close,
    PSP_VPFE_TVP5146_Control,
  },
  0                       //segId
};

static PSP_VPBEOsdConfigParams  vid0Params = {
  PSP_VPSS_FRAME_MODE,                // ffmode
  PSP_VPSS_BITS16,                    // bitsPerPixel
  //PSP_VPBE_RGB_888,  //ajout TG
 PSP_VPBE_YCbCr422,                  // colorFormat
  (720 *  (16/8u)),                   // pitch
  0,                                  // leftMargin
  0,                                  // topMargin
  720,                                // width
  480,                                // height
  0,                                  // segId
  PSP_VPBE_ZOOM_IDENTITY,             // hScaling
  PSP_VPBE_ZOOM_IDENTITY,             // vScaling
  PSP_VPBE_EXP_IDENTITY,              // hExpansion
  PSP_VPBE_EXP_IDENTITY,              // vExpansion
  NULL                                // appCallback
};

static PSP_VPBEVencConfigParams vencParams = {
  PSP_VPBE_DISPLAY_NTSC_INTERLACED_COMPOSITE // Display Standard
};


/*********************************************************/
/********************** OWN DEFINES **********************/
/*********************************************************/

#define WIDTH  720
#define HEIGHT 480
#define GAMMA  30
#define THRESHOLD 15
#define PI 3.14159
#define LOOKUP_SIZE 180
#define MAX_VALUE_BIN 0xFF
#define MIN_VALUE_BIN 0x00
#define POW_G 8

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#pragma DATA_SECTION(imageInStack, ".stack")
uint8_t imageInStack[WIDTH*HEIGHT];
#pragma DATA_SECTION(imageOutStack, ".stack")
uint8_t imageOutStack[WIDTH*HEIGHT];

void sobelFilter(const uint8_t *in_data_, uint8_t *out_data, uint16_t cols , uint16_t rows);
void dericheGL(uint8_t* in, uint8_t* out);
void smoothing(uint8_t* img, uint8_t* C, uint8_t* A);
void smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2);
void associateMem();
void freeMem();

uint8_t* causal_;
uint8_t* anticausal_;
uint8_t* tmp_;
uint8_t* acc_;
uint8_t* lutCos_;
uint8_t* lutSin_;

/*********************************************************/
/********************** END DEFINES **********************/
/*********************************************************/

void start_boucle() {
  PSP_VPBEChannelParams beinitParams;
  PSP_VPFEChannelParams feinitParams;
  GIO_Attrs gioAttrs = GIO_ATTRS;
  PSP_VPSSSurfaceParams *FBAddr = NULL;
  Uint32 i = 0;
  Uint32 numOfIterations = 10000;


  //Init CSL du DMA
  edma3init();


  // Create ccdc channel
  feinitParams.id = PSP_VPFE_CCDC;
  feinitParams.params = (PSP_VPFECcdcConfigParams*)&ccdcParams;
  ccdcHandle = FVID_create( "/VPFE0", IOM_INOUT, NULL, &feinitParams,
                            &gioAttrs);
  if ( NULL == ccdcHandle) {
    return;
  }

  // Configure the TVP5146 video decoder
  if( FVID_control( ccdcHandle,
                    VPFE_ExtVD_BASE + PSP_VPSS_EXT_VIDEO_DECODER_CONFIG,
                    &tvp5146Params) != IOM_COMPLETED ) {
	return;
  } else {
    for ( i=0; i < NO_OF_BUFFERS; i++ ) {
      if ( IOM_COMPLETED == FVID_alloc( ccdcHandle, &ccdcAllocFB[i] ) ) {
        if ( IOM_COMPLETED != FVID_queue(ccdcHandle, ccdcAllocFB[i] ) ) {
          return;
        }
      }
    }
  }

  // Create video channel
  beinitParams.id = PSP_VPBE_VIDEO_0;
  beinitParams.params = (PSP_VPBEOsdConfigParams*)&vid0Params;
  vid0Handle = FVID_create( "/VPBE0", IOM_INOUT,NULL, &beinitParams,
                            &gioAttrs );
  if ( NULL == vid0Handle ) {
    return;
  } else {
    for ( i=0; i<NO_OF_BUFFERS; i++ )  {
      if ( IOM_COMPLETED == FVID_alloc( vid0Handle, &vidAllocFB[i] ) ) {
        if ( IOM_COMPLETED != FVID_queue( vid0Handle, vidAllocFB[i]) ) {
          return;
        }
      }
    }
  }

  // Create venc channel
  beinitParams.id = PSP_VPBE_VENC;
  beinitParams.params = (PSP_VPBEVencConfigParams *)&vencParams;
  vencHandle = FVID_create( "/VPBE0", IOM_INOUT, NULL, &beinitParams,
                            &gioAttrs);
  if ( NULL == vencHandle ) {
    return;
  }

  //Allocation memoire et la structure qui contiendra l'image
  FVID_alloc( ccdcHandle, &FBAddr );

  //================BOUCLE ACQUISITION & COPIE & AFFICHAGE DES IMAGES========================
	FVID_alloc( ccdcHandle, &FBAddr );
	PSP_VPSSSurfaceParams *FBAddrImageOut = NULL;
	FVID_alloc(ccdcHandle, &FBAddrImageOut);

	associateMem();
	for( i = 0; i < numOfIterations; i++ ) {
		if ( IOM_COMPLETED != FVID_exchange( ccdcHandle, &FBAddr ) ) {
			return;
		}

		int x, y;
		uint16_t *imageOut = FBAddrImageOut->frameBufferPtr;
		uint16_t *imageIn = FBAddr->frameBufferPtr;

		for(x = 0; x < WIDTH; x++){
			for(y = 0; y < HEIGHT; y++){
				imageInStack[x + y * WIDTH] = (uint8_t)((imageIn[x + y * WIDTH] & 0xFF00) >> 8);
			}
		}

		dericheGL(imageInStack, imageOutStack);
		//sobelFilter(imageInStack, imageOutStack, WIDTH, HEIGHT);

		for(x = 0; x < WIDTH; x++){
			for(y = 0; y < HEIGHT; y++){
				imageOut[x + y * WIDTH] = (((uint16_t)(imageOutStack[x + y * WIDTH])) << 8) | 0x0080;
			}
		}

		LOG_printf( &trace, "Printing iteration number %u", i );

		if ( IOM_COMPLETED != FVID_exchange( vid0Handle, &FBAddrImageOut ) ) {
			return;
		}
	}
	freeMem();

  //================FIN BOUCLE ACQUISITION & COPIE & AFFICHAGE DES IMAGES======================
  FVID_free(vid0Handle,  FBAddr);

  // Free Memory Buffers
  for( i=0; i< NO_OF_BUFFERS; i++ ) {
    FVID_free( ccdcHandle, ccdcAllocFB[i] );
    FVID_free( vid0Handle, vidAllocFB[i] );
  }

  // Delete Channels
  FVID_delete( ccdcHandle );
  FVID_delete( vid0Handle );
  FVID_delete( vencHandle );

  return;
}

void sobelFilter(const uint8_t *in_data_, uint8_t *out_data, uint16_t cols , uint16_t rows){
	int8_t* in_data = (int8_t*)in_data_;
	uint16_t x,y;
	for(y = 1; y < rows-1; y++){
		for(x = 1; x < cols-1; x++){
			uint8_t G;
			int Gx, Gy;
			Gx =
				-in_data[(x-1) + (y-1) * cols]
				+in_data[(x+1) + (y-1) * cols]

				-2*in_data[(x-1) + y * cols]
				+2*in_data[(x+1) + y * cols]

				-in_data[(x-1) + (y+1) * cols]
				+in_data[(x+1) + (y+1) * cols];

			Gy =
				+in_data[(x-1) + (y-1) * cols]
				+2*in_data[x + (y-1) * cols]
				+in_data[(x+1) + (y-1) * cols]

				-in_data[(x-1) + (y+1) * cols]
				-2*in_data[x + (y+1) * cols]
				-in_data[(x+1) + (y+1) * cols];

			if(Gx < 0){
				Gx = -Gx;
			}

			if(Gy < 0){
				Gy = -Gy;
			}

			G = (uint8_t)Gx + (uint8_t)Gy;
			if(G > 128){
				G = 0xFF;
			}
			else{
				G = 0x00;
			}
			out_data[x + y * cols] = G;
		}
	}
}

void dericheGL(uint8_t* im_in, uint8_t* im_out)
{
	uint8_t* ptrAn, * ptrTmp, * ptrCa, * ptrOut;
    size_t i, ilen;
    int dx, dy;

    smoothing(im_in, causal_, anticausal_);

    ptrAn  = &anticausal_[0];
    ptrTmp = &tmp_[0];
    ptrCa  = &causal_[0];

    for(i = 0, ilen = HEIGHT * WIDTH; i < ilen; ++i){
        if(*ptrAn > 255.0){
            *ptrTmp = 255.0;
        }
        else{
            *ptrTmp = *ptrAn;
        }
        *ptrCa = *ptrTmp;

        ptrAn++;
        ptrTmp++;
        ptrCa++;
    }

    smoothing(tmp_, causal_, anticausal_);

    ptrAn  = &anticausal_[0];
    ptrOut = &im_out[0];

    size_t maxrho  = (sqrt(2) * MAX(WIDTH, HEIGHT));
    memset(acc_, 0, maxrho*180);

    for(i = 0, ilen = HEIGHT * WIDTH; i < ilen; ++i){
        unsigned int sum;
        uint8_t theta, rho;

        dx = (
            - *(ptrAn)
            + *(ptrAn + 1)
            - *(ptrAn + WIDTH)
            + *(ptrAn + WIDTH + 1)
        );

        dy =(
            - *(ptrAn)
            + *(ptrAn + WIDTH)
            - *(ptrAn + 1)
            + *(ptrAn + WIDTH + 1)
        );

        sum = abs(dx)+abs(dy);

        // TODO remove binarisation (still there for tests)
        if(sum >= THRESHOLD){
            *ptrOut = MAX_VALUE_BIN;
        }
        else{
            *ptrOut = MIN_VALUE_BIN;
        }

        // TODO what shoul I do when dx=0 ?
       if(sum > THRESHOLD && dx != 0){
           size_t x = i%WIDTH;
           size_t y = ((i-i%WIDTH)/WIDTH);

           theta = atan(dy/dx)*180/PI;
           rho   = x*lutCos_[theta] + y*lutSin_[theta];
           acc_[rho*WIDTH+theta] = acc_[rho*180+theta]+1;
       }

        ptrAn++;
        ptrOut++;
    }
}

void smoothing(uint8_t* img, uint8_t* C, uint8_t* A)
{
	int g  = GAMMA;
    int g1 = ((1<<POW_G) - g)*((1<<POW_G) - g);
    int g2 = g<<1;
    int gg = g*g;
    int i;

    uint8_t powerOfG2 = POW_G<<1;

    for(i = 0; i < HEIGHT; i+=4){
        smoothingLoopContent(img, C, A, g1, g2, gg, i+0, (i+0)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+1, (i+1)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+2, (i+2)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+3, (i+3)*WIDTH, powerOfG2);
    }
}


void smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2)
{
    int j;

    uint8_t* ptrI = &img[iw];

    uint8_t* ptrC = &C[iw];
    uint8_t* ptrCm2, * ptrCm1, * ptrC0, * ptrC1, * ptrC2;

    uint8_t* ptrA;
    uint8_t* ptrA1, * ptrA2, * ptrA3, * ptrA4;

    *ptrC++ = (g1*(*ptrI++))>>powerOfG2;
    *ptrC   = (g1*(*ptrI++) + g2*(*(ptrC-1) ))>>powerOfG2;
    ptrC++;

    for(j = 2; j < WIDTH; j+=4){
        ptrCm2 = ptrC-2;
        ptrCm1 = ptrC-1;
        ptrC0 = ptrC;
        ptrC1 = ptrC+1;
        ptrC2 = ptrC+2;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrCm1)
            + gg * (*ptrCm2)
        )>>powerOfG2;

        ptrI++;

        *ptrC1 = (
              g1 * (*ptrI)
            + g2 * (*ptrC0)
            + gg * (*ptrCm1)
        )>>powerOfG2;

        ptrI++;

        *ptrC2 = (
              g1 * (*ptrI)
            + g2 * (*ptrC1)
            + gg * (*ptrC0)
        )>>powerOfG2;

        ptrC+=3;
        ptrI++;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrC2)
            + gg * (*ptrC1)
        )>>powerOfG2;

        ptrC++;
        ptrI++;
    }

    ptrA = &A[iw + WIDTH - 1];
    ptrC = &C[iw - 1];

    *ptrA-- = (g1 * (*ptrC--))>>powerOfG2;
    *ptrA   = (g1 * (*ptrC--) + g2 * (*ptrA))>>powerOfG2;
    ptrA--;

    for(j = WIDTH - 3; j>=0; j-=4){
        ptrA1 = ptrA+1;
        ptrA2 = ptrA+2;
        ptrA3 = ptrA+3;
        ptrA4 = ptrA+4;

        *ptrA = (
              g1 * (*ptrC)
            + g2 * (*ptrA1)
            + gg * (*ptrA2)
        )>>powerOfG2;

        ptrC++;

        *ptrA1 = (
              g1 * (*ptrC)
            + g2 * (*ptrA2)
            + gg * (*ptrA3)
        )>>powerOfG2;

        ptrC++;

        *ptrA2 = (
              g1 * (*ptrC)
            + g2 * (*ptrA3)
            + gg * (*ptrA4)
        )>>powerOfG2;

        ptrC++;

        *ptrA3 = (
              g1 * (*ptrC)
            + g2 * (*ptrA4)
            + gg * (*(ptrA + 5))
        )>>powerOfG2;

        ptrC++;
        ptrA -= 4;
    }
}


void associateMem()
{
	size_t i;

	causal_     = (uint8_t*) malloc(WIDTH*HEIGHT*sizeof(uint8_t));
    anticausal_ = (uint8_t*) malloc(WIDTH*HEIGHT*sizeof(uint8_t));
    tmp_        = (uint8_t*)malloc(WIDTH*HEIGHT*sizeof(uint8_t));

    size_t maxrho = (sqrt(2) * MAX(WIDTH, HEIGHT));
    acc_          = (uint8_t*) malloc(maxrho*180*sizeof(uint8_t));

    lutCos_ = (uint8_t*) malloc(LOOKUP_SIZE*sizeof(uint8_t));
    lutSin_ = (uint8_t*) malloc(LOOKUP_SIZE*sizeof(uint8_t));

    for(i=0; i<LOOKUP_SIZE; ++i){
        lutCos_[i] = cos(i)*180 / PI;
        lutSin_[i] = sin(i)*180 / PI;
    }
}

void freeMem()
{
	free(tmp_);
    free(anticausal_);
    free(causal_);
    free(acc_);
    free(lutCos_);
    free(lutSin_);
}
