/* T. Grandpierre :
Version initiale qui ne fait que acquisition YUV puis affichage
*/

#include <std.h>
#include <gio.h>
#include <log.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "psp_vpfe.h"
#include "psp_vpbe.h"
#include "fvid.h"
#include "psp_tvp5146_extVidDecoder.h"

#include <soc.h>
#include <cslr_ccdc.h>

extern LOG_Obj trace;  // BIOS LOG object

#define NO_OF_BUFFERS       (2u)

// Global Variable Defined
static PSP_VPSSSurfaceParams *ccdcAllocFB[NO_OF_BUFFERS]={NULL};
static PSP_VPSSSurfaceParams *vidAllocFB[NO_OF_BUFFERS] ={NULL};

static FVID_Handle   ccdcHandle;
static FVID_Handle   vid0Handle;
static FVID_Handle   vencHandle;

static PSP_VPFE_TVP5146_ConfigParams tvp5146Params = {
  TRUE, // enable656Sync
  PSP_VPFE_TVP5146_FORMAT_COMPOSITE, // format
  PSP_VPFE_TVP5146_MODE_AUTO         // mode
};

static PSP_VPFECcdcConfigParams ccdcParams = {
  PSP_VPFE_CCDC_YCBCR_8,  // dataFlow
  PSP_VPSS_FRAME_MODE,    // ffMode
  480,                    // height
  720,                    // width
  (720 *2),               // pitch
  0,                      // horzStartPix
  0,                      // vertStartPix
  NULL,                   // appCallback
  {
    PSP_VPFE_TVP5146_Open, // extVD Fxn
    PSP_VPFE_TVP5146_Close,
    PSP_VPFE_TVP5146_Control,
  },
  0                       //segId
};

static PSP_VPBEOsdConfigParams  vid0Params = {
  PSP_VPSS_FRAME_MODE,                // ffmode
  PSP_VPSS_BITS16,                    // bitsPerPixel
  //PSP_VPBE_RGB_888,  //ajout TG
 PSP_VPBE_YCbCr422,                  // colorFormat
  (720 *  (16/8u)),                   // pitch
  0,                                  // leftMargin
  0,                                  // topMargin
  720,                                // width
  480,                                // height
  0,                                  // segId
  PSP_VPBE_ZOOM_IDENTITY,             // hScaling
  PSP_VPBE_ZOOM_IDENTITY,             // vScaling
  PSP_VPBE_EXP_IDENTITY,              // hExpansion
  PSP_VPBE_EXP_IDENTITY,              // vExpansion
  NULL                                // appCallback
};

static PSP_VPBEVencConfigParams vencParams = {
  PSP_VPBE_DISPLAY_NTSC_INTERLACED_COMPOSITE // Display Standard
};


/*********************************************************/
/********************** OWN DEFINES **********************/
/*********************************************************/

#define WIDTH  720
#define HEIGHT 480
#define GAMMA  30
#define THRESHOLD 15
#define POW_G 8
#define MAX_VALUE_BIN 0xFF
#define MIN_VALUE_BIN 0x00
#define LOOKUP_SIZE 361
#define VOTES_LINE 60
#define PI 3.14159

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#pragma DATA_SECTION(imageInStack, ".stack")
uint8_t imageInStack[WIDTH*HEIGHT];
#pragma DATA_SECTION(imageOutStack, ".stack")
uint8_t imageOutStack[WIDTH*HEIGHT];

void dericheGL(uint8_t* in, uint8_t* out);
void smoothing(uint8_t* img, uint8_t* C, uint8_t* A);
void smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2);
void associateMem();
void hough(uint8_t* im_out);
int lutArctan(double angle);
void freeMem();

uint8_t* causal_;
uint8_t* anticausal_;
uint8_t* tmp_;
uint8_t* acc_;
int* lutCos_;
int* lutSin_;
int* lutArctan_;

/*********************************************************/
/********************** END DEFINES **********************/
/*********************************************************/

void start_boucle() {
  PSP_VPBEChannelParams beinitParams;
  PSP_VPFEChannelParams feinitParams;
  GIO_Attrs gioAttrs = GIO_ATTRS;
  PSP_VPSSSurfaceParams *FBAddr = NULL;
  Uint32 i = 0;
  Uint32 numOfIterations = 10000;


  //Init CSL du DMA
  edma3init();


  // Create ccdc channel
  feinitParams.id = PSP_VPFE_CCDC;
  feinitParams.params = (PSP_VPFECcdcConfigParams*)&ccdcParams;
  ccdcHandle = FVID_create( "/VPFE0", IOM_INOUT, NULL, &feinitParams,
                            &gioAttrs);
  if ( NULL == ccdcHandle) {
    return;
  }

  // Configure the TVP5146 video decoder
  if( FVID_control( ccdcHandle,
                    VPFE_ExtVD_BASE + PSP_VPSS_EXT_VIDEO_DECODER_CONFIG,
                    &tvp5146Params) != IOM_COMPLETED ) {
	return;
  } else {
    for ( i=0; i < NO_OF_BUFFERS; i++ ) {
      if ( IOM_COMPLETED == FVID_alloc( ccdcHandle, &ccdcAllocFB[i] ) ) {
        if ( IOM_COMPLETED != FVID_queue(ccdcHandle, ccdcAllocFB[i] ) ) {
          return;
        }
      }
    }
  }

  // Create video channel
  beinitParams.id = PSP_VPBE_VIDEO_0;
  beinitParams.params = (PSP_VPBEOsdConfigParams*)&vid0Params;
  vid0Handle = FVID_create( "/VPBE0", IOM_INOUT,NULL, &beinitParams,
                            &gioAttrs );
  if ( NULL == vid0Handle ) {
    return;
  } else {
    for ( i=0; i<NO_OF_BUFFERS; i++ )  {
      if ( IOM_COMPLETED == FVID_alloc( vid0Handle, &vidAllocFB[i] ) ) {
        if ( IOM_COMPLETED != FVID_queue( vid0Handle, vidAllocFB[i]) ) {
          return;
        }
      }
    }
  }

  // Create venc channel
  beinitParams.id = PSP_VPBE_VENC;
  beinitParams.params = (PSP_VPBEVencConfigParams *)&vencParams;
  vencHandle = FVID_create( "/VPBE0", IOM_INOUT, NULL, &beinitParams,
                            &gioAttrs);
  if ( NULL == vencHandle ) {
    return;
  }

  //Allocation memoire et la structure qui contiendra l'image
  FVID_alloc( ccdcHandle, &FBAddr );

  //================BOUCLE ACQUISITION & COPIE & AFFICHAGE DES IMAGES========================
	FVID_alloc( ccdcHandle, &FBAddr );
	PSP_VPSSSurfaceParams *FBAddrImageOut = NULL;
	FVID_alloc(ccdcHandle, &FBAddrImageOut);

	associateMem();
	for( i = 0; i < numOfIterations; i++ ) {
		if ( IOM_COMPLETED != FVID_exchange( ccdcHandle, &FBAddr ) ) {
			return;
		}

		int x, y;
		uint16_t *imageOut = FBAddrImageOut->frameBufferPtr;
		uint16_t *imageIn = FBAddr->frameBufferPtr;

		for(x = 0; x < WIDTH; x++){
			for(y = 0; y < HEIGHT; y++){
				imageInStack[x + y * WIDTH] = (uint8_t)((imageIn[x + y * WIDTH] & 0xFF00) >> 8);
			}
		}

		dericheGL(imageInStack, imageOutStack);
        hough(imageOutStack);

		for(x = 0; x < WIDTH; x++){
			for(y = 0; y < HEIGHT; y++){
				imageOut[x + y * WIDTH] = (((uint16_t)(imageOutStack[x + y * WIDTH])) << 8) | 0x0080;
			}
		}

		LOG_printf( &trace, "Printing iteration number %u", i );

		if ( IOM_COMPLETED != FVID_exchange( vid0Handle, &FBAddrImageOut ) ) {
			return;
		}
	}
	freeMem();

  //================FIN BOUCLE ACQUISITION & COPIE & AFFICHAGE DES IMAGES======================
  FVID_free(vid0Handle,  FBAddr);

  // Free Memory Buffers
  for( i=0; i< NO_OF_BUFFERS; i++ ) {
    FVID_free( ccdcHandle, ccdcAllocFB[i] );
    FVID_free( vid0Handle, vidAllocFB[i] );
  }

  // Delete Channels
  FVID_delete( ccdcHandle );
  FVID_delete( vid0Handle );
  FVID_delete( vencHandle );

  return;
}

void dericheGL(uint8_t* im_in, uint8_t* im_out)
{
	uint8_t* ptrAn, * ptrTmp, * ptrCa, * ptrOut;
    size_t i, ilen;
    int dx, dy;

    smoothing(im_in, causal_, anticausal_);

    ptrAn  = &anticausal_[0];
    ptrTmp = &tmp_[0];
    ptrCa  = &causal_[0];

    for(i = 0, ilen = HEIGHT * WIDTH; i < ilen; ++i){
        if(*ptrAn > 255.0){
            *ptrTmp = 255.0;
        }
        else{
            *ptrTmp = *ptrAn;
        }
        *ptrCa = *ptrTmp;

        ptrAn++;
        ptrTmp++;
        ptrCa++;
    }

    smoothing(tmp_, causal_, anticausal_);

    ptrAn  = &anticausal_[0];
    ptrOut = &im_out[0];

    size_t maxrho  = (sqrt(2) * MAX(WIDTH, HEIGHT));
    memset(acc_, 0, maxrho*180);

    for(i = 0, ilen = HEIGHT * WIDTH; i < ilen; ++i){
        unsigned int sum;
        uint8_t theta, rho;

        dx = (
            - *(ptrAn)
            + *(ptrAn + 1)
            - *(ptrAn + WIDTH)
            + *(ptrAn + WIDTH + 1)
        );

        dy =(
            - *(ptrAn)
            + *(ptrAn + WIDTH)
            - *(ptrAn + 1)
            + *(ptrAn + WIDTH + 1)
        );

        sum = abs(dx)+abs(dy);

        *ptrOut = im_in[i];

        if(sum > threshold_ && dx != 0){
            size_t x = i%WIDTH;
            size_t y = ((i-x)/WIDTH);

            theta = lutArctan(double(dy)/double(dx));

            rho   = ((x<<8)*lutCos_[theta] + (y<<8)*lutSin_[theta])>>16;
            rhoIdx = ((rho>>1) + (maxrho>>1));

            if(rhoIdx > 0){
                uint8_t accVal = acc_[rhoIdx*180+theta]+1;
                acc_[rhoIdx*180+theta] = accVal;
                maxVal = accVal > maxVal ? accVal : maxVal;
            }
        }

        ptrAn++;
        ptrOut++;
    }

    for(i = 0; i<180*maxrho; i++){
        acc_[i] = acc_[i]*255/maxVal;
    }
}

void smoothing(uint8_t* img, uint8_t* C, uint8_t* A)
{
	int g  = GAMMA;
    int g1 = ((1<<POW_G) - g)*((1<<POW_G) - g);
    int g2 = g<<1;
    int gg = g*g;
    int i;

    uint8_t powerOfG2 = POW_G<<1;

    for(i = 0; i < HEIGHT; i+=4){
        smoothingLoopContent(img, C, A, g1, g2, gg, i+0, (i+0)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+1, (i+1)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+2, (i+2)*WIDTH, powerOfG2);
        smoothingLoopContent(img, C, A, g1, g2, gg, i+3, (i+3)*WIDTH, powerOfG2);
    }
}

void smoothingLoopContent(uint8_t* img, uint8_t* C, uint8_t*A, int g1, int g2, int gg, int i, size_t iw, uint8_t powerOfG2)
{
    int j;

    uint8_t* ptrI = &img[iw];

    uint8_t* ptrC = &C[iw];
    uint8_t* ptrCm2, * ptrCm1, * ptrC0, * ptrC1, * ptrC2;

    uint8_t* ptrA;
    uint8_t* ptrA1, * ptrA2, * ptrA3, * ptrA4;

    *ptrC++ = (g1*(*ptrI++))>>powerOfG2;
    *ptrC   = (g1*(*ptrI++) + g2*(*(ptrC-1) ))>>powerOfG2;
    ptrC++;

    for(j = 2; j < WIDTH; j+=4){
        ptrCm2 = ptrC-2;
        ptrCm1 = ptrC-1;
        ptrC0 = ptrC;
        ptrC1 = ptrC+1;
        ptrC2 = ptrC+2;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrCm1)
            + gg * (*ptrCm2)
        )>>powerOfG2;

        ptrI++;

        *ptrC1 = (
              g1 * (*ptrI)
            + g2 * (*ptrC0)
            + gg * (*ptrCm1)
        )>>powerOfG2;

        ptrI++;

        *ptrC2 = (
              g1 * (*ptrI)
            + g2 * (*ptrC1)
            + gg * (*ptrC0)
        )>>powerOfG2;

        ptrC+=3;
        ptrI++;

        *ptrC = (
              g1 * (*ptrI)
            + g2 * (*ptrC2)
            + gg * (*ptrC1)
        )>>powerOfG2;

        ptrC++;
        ptrI++;
    }

    ptrA = &A[iw + WIDTH - 1];
    ptrC = &C[iw - 1];

    *ptrA-- = (g1 * (*ptrC--))>>powerOfG2;
    *ptrA   = (g1 * (*ptrC--) + g2 * (*ptrA))>>powerOfG2;
    ptrA--;

    for(j = WIDTH - 3; j>=0; j-=4){
        ptrA1 = ptrA+1;
        ptrA2 = ptrA+2;
        ptrA3 = ptrA+3;
        ptrA4 = ptrA+4;

        *ptrA = (
              g1 * (*ptrC)
            + g2 * (*ptrA1)
            + gg * (*ptrA2)
        )>>powerOfG2;

        ptrC++;

        *ptrA1 = (
              g1 * (*ptrC)
            + g2 * (*ptrA2)
            + gg * (*ptrA3)
        )>>powerOfG2;

        ptrC++;

        *ptrA2 = (
              g1 * (*ptrC)
            + g2 * (*ptrA3)
            + gg * (*ptrA4)
        )>>powerOfG2;

        ptrC++;

        *ptrA3 = (
              g1 * (*ptrC)
            + g2 * (*ptrA4)
            + gg * (*(ptrA + 5))
        )>>powerOfG2;

        ptrC++;
        ptrA -= 4;
    }
}

void hough(uint8_t* im_out)
{
    size_t maxrho = ceil(sqrt(WIDTH*WIDTH + HEIGHT*HEIGHT));

	im_out = (uint8_t*)(out.data);

    for(size_t i = 0; i < maxrho; i++){
        for(size_t j = 1; j<180; j++){
            if(acc_[i*180+j] > VOTES_LINE){
                int rho  = ((i<<1) - maxrho)<<8;
                int a = - (lutCos_[j] / lutSin_[j])<<8;
                int b = (rho / lutSin_[j])<<8;

                if(j >= 45 && j <= 135){
                    for(size_t x=0; x<WIDTH; x++){
                        int y = (a*x+b)>>8;
                        if(y>0 && y<=HEIGHT){
                            im_out[y*WIDTH+x] = 255;
                        }
                    }
                }
                else{
                    for(size_t y=0; y<HEIGHT; y++){
                        int x = 0;
                        if(a != 0){
                            x = ((y<<8)-b)/a;
                        }
                        if(x>=0 && x<WIDTH){
                            im_out[y*WIDTH+x] = 255;
                        }
                    }
                }
            }
        }
    }
}

int lutArctan(double angle)
{
    if(angle < 0.009 && angle > -0.009){
        return 0;
    }
    else if(angle < 1 && angle > -1){
        return atan(angle)*180/PI;
    }
    else if(angle > 115){
        return 90;
    }
    else if(angle < -115){
        return -90;
    }
    else{
        return lutArctan_[int(angle)];
    }
}

void associateMem()
{
	size_t i;

	causal_     = (uint8_t*) malloc(WIDTH*HEIGHT*sizeof(uint8_t));
    anticausal_ = (uint8_t*) malloc(WIDTH*HEIGHT*sizeof(uint8_t));
    tmp_        = (uint8_t*)malloc(WIDTH*HEIGHT*sizeof(uint8_t));

    size_t maxrho = (sqrt(2) * MAX(WIDTH, HEIGHT));
    acc_          = (uint8_t*) malloc(maxrho*180*sizeof(uint8_t));

    lutCos_ = (int*) malloc(LOOKUP_SIZE*sizeof(int));
    lutSin_ = (int*) malloc(LOOKUP_SIZE*sizeof(int));
    lutArctan_ = (int*) malloc(231*sizeof(int));

    for(int i=-180; i<180; i++){
        lutCos_[i] = cos(i*PI/180)*256;
        lutSin_[i] = sin(i*PI/180)*256;
    }

    for(int i = -115; i< 116; i++){
        lutArctan_[i] = atan(i)*180/PI;
    }
}

void freeMem()
{
	free(tmp_);
    free(anticausal_);
    free(causal_);
    free(acc_);
    free(lutCos_);
    free(lutSin_);
}
